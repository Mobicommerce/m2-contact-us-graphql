<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_CmsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\ContactUsGraphQl\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;


use Magento\Framework\Exception\MailException;
use Magento\Framework\App\Area;
use Magento\Store\Model\Store;

class SendMessage implements ResolverInterface
{
    public function __construct(
        \FME\Contactus\Model\ContactFactory $contactModel,
        \FME\Contactus\Helper\Data $_mymoduleHelper,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    ) {
        $this->contactModel = $contactModel->create();
        $this->_mymoduleHelper = $_mymoduleHelper;
        $this->transportBuilder = $transportBuilder;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {

        //echo "<pre>"; print_r($args);exit;
     
        if (empty($args['input']['name'])) {
            throw new GraphQlInputException(__('"name" value should be specified'));
        }

        if (empty($args['input']['email'])) {
            throw new GraphQlInputException(__('"email" value should be specified'));
        }

        if (empty($args['input']['phone'])) {
            throw new GraphQlInputException(__('"phone" value should be specified'));
        }

        if (empty($args['input']['subject'])) {
            throw new GraphQlInputException(__('"subject" value should be specified'));
        }

        if (empty($args['input']['message'])) {
            throw new GraphQlInputException(__('"message" value should be specified'));
        }

        
        
        
        $status = false;

        try {
            $this->sendMail($args['input']);
        } catch (NoSuchEntityException $e) {
            throw new GraphQlInputException(__('Some issue in sending mail'));
        }
        return ["result"=>["status"=>true,"message"=>__('Thanks for Contacting Us! We will connect with you.')]];
        //return $data;
    }

    function sendMail($data)
    {
       
        $suggestData = [
            'type' => $data['type'],
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'subject' => $data['subject'],
            'message' => $data['message'],
        ];

         
          $this->contactModel->setData($suggestData);
          $this->contactModel->save();

          $postObject = new \Magento\Framework\DataObject();
          $postObject->setData($suggestData);
           
        if ($this->_mymoduleHelper->getreceipt() !='') {
                    $transport =  $this->transportBuilder
                    ->setTemplateIdentifier($this->_mymoduleHelper->getemailtemplate())
                    ->setTemplateOptions(
                        [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                        ]
                    )
                    ->setTemplateVars(['data' => $postObject])
                    ->setFrom($this->_mymoduleHelper->getemailsender())
                    ->addTo($this->_mymoduleHelper->getreceipt())
                    ->setReplyTo($suggestData['email'])
                    ->getTransport();
                    $transport->sendMessage();
        }
          
          return true;
    }
}
